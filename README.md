# About

CxFlow-front-api is a server that hosted CxFlow and will expose it via API Endpoint.
This project was created to allow the use of CxFlow outside of pipelines with orechestrator that do not support WebHook (jenkins)


# Installation

CxFlow-front-api should be installed using docker
First you have to clone the repository
```shell
git clone https://gitlab.com/svevia/cxflow-front-api.git
cd cxflow-front-api
```

Then you have to setup a `cxflow-application.yml.template` by copying `cxflow-application.yml.template1` and replace variables with your data

```shell
cp conf/cxflow-application.yml.template1 conf/cxflow-application.yml.template
```
Check `conf/config.yml` and update the "token" value

Next you can build the docker

```shell
docker build --tag cxflow-front-api .
docker run -p 9000:9000 --name cxflow-front-api cxflow-front-api:latest
```


# Usage
When the server is running you can first connect to `http://localhost:9000/info` you should see 
```plaintext
{"success": true, "message": "It Works !"}
```

Depending on your usage you could then use following endpoints to interact with CxFlow

| Endpoint   | Method     | Information                                       | Required Data                |
|:-----------|:-----------|:--------------------------------------------------|:-----------------------------|
| /project   | POST       | Run CxFlow in project mode                        | Token, appName               |
| /scan      | POST       | Run CxFlow in scan mode                           | Token, appName, code (zip)   |
| /scan/async| POST       | Run CxFlow in scan mode without waiting output    | Token, appName, code (zip)   |

# Jenkins pipeline exemple

Here is a basic Jenkins pipeline exemple for using this project
It's zipping source code and send it to cxflow-front-api

```plaintext
node{
    if(fileExists("test.zip")){
        sh 'rm test.zip'
    }
    zip dir: "${workspace}", zipFile: "test.zip"
    sh 'curl --fail -X POST -F appName="${JOB_NAME}" -F code="@test.zip" -F token="test" <URL>/<ENDPOINT>'
}
```

# Roadmap
 - Implement HTTPS
 - Add new endpoint for others CxFlow usage
 - Allow dynmaic update of cxflow-application.yml


