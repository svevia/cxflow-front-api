FROM openjdk:8-jre-alpine

RUN apk update && apk upgrade

RUN apk add python3 py3-pip
RUN pip3 install --upgrade pip

COPY . .

RUN pip3 install -r requirements.txt 

ENTRYPOINT ["python3", "front-api.py"]
EXPOSE 9000