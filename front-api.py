import subprocess
import requests
import yaml
import zipfile
import os
import uuid
import shutil
import time
from bottle import route, run, Bottle, request, static_file, response, HTTPResponse, abort, HTTPError
import threading
from queue import Queue
from concurrent.futures import ProcessPoolExecutor
from waitress import serve


app = Bottle()

def security():
	'''
	Funtion checking for the token on each api endpoints, except on "/info"
	'''
	if request.forms.get('token') == None or request.forms.get('token') != config['token']:
		return {"success": False,"error": "Bad Token"}
	else:
		return True



#------------------------- ENDPOINTS ----------------------#
@app.route('/info', method='GET')
def info():
	return """{"success": true, "message": "It Works !"}"""


@app.route('/project', method='POST')
def project():
	sec = security()
	if sec != True:
		print(sec, flush=True)
		response.status = 403
		return sec
	else:
		appName = request.forms.get("appName")
		prepareConfig()
		return startCxFlowProjectMode(applicationConfPath,appName)



@app.route('/scan', method='POST')
def scan():
	sec = security()
	if sec != True:
		print(sec, flush=True)
		response.status = 403
		return sec
	else:
		appName = request.forms.get("appName")
		file = request.files.get("code")
		uniqueId = uuid.uuid1()
		file.save("/tmp/%s.zip" % uniqueId)
		with zipfile.ZipFile("/tmp/%s.zip" % uniqueId,"r") as zip_ref:
			zip_ref.extractall("/tmp/%s" % uniqueId)
		prepareConfig()
		return startCxFlowScanMode(applicationConfPath,appName,uniqueId)

@app.route('/scan/async', method='POST')
def scan():
	sec = security()
	if sec != True:
		print(sec, flush=True)
		response.status = 403
		return sec
	else:
		appName = request.forms.get("appName")
		file = request.files.get("code")
		uniqueId = uuid.uuid1()
		file.save("/tmp/%s.zip" % uniqueId)
		with zipfile.ZipFile("/tmp/%s.zip" % uniqueId,"r") as zip_ref:
			zip_ref.extractall("/tmp/%s" % uniqueId)
		prepareConfig()
		data = {}
		data["config"] = applicationConfPath
		data["appName"] = appName
		data["id"] = uniqueId
		q.put(data)
		return HTTPResponse(body="{'status': 'job accepted'}", status=200)

#-----------------------------------------------------------#


def cleanWorkspace(uniqueId):
	os.remove("/tmp/%s.zip" % uniqueId)
	shutil.rmtree("/tmp/%s" % uniqueId)

def runCxFlow(cmd):
	print("running : %s" % cmd, flush=True)
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True,universal_newlines=True)
	output, error = proc.communicate()
	returncode = proc.returncode
	print(output, flush=True)
	return output, returncode

def startCxFlowScanMode(config, appName, path):
	cmd = """/usr/bin/java -jar %s --spring.config.location=%s """ % (cxFlowJar, config)+\
          """--scan """+\
          """--cx-project="%s" """ % (appName)+\
          """--app="%s" """ % (appName)+\
          """--f=/tmp/%s""" % (path)

	output, returncode = runCxFlow(cmd)
	cleanWorkspace(path)
	if returncode != 0:
		return HTTPError(body=output, status=520)
	else:
		return HTTPResponse(body=output, status=200)



def startCxFlowProjectMode(config, appName):
	cmd = """/usr/bin/java -jar %s --spring.config.location=%s """ % (cxFlowJar, config)+\
          """--project """+\
          """--cx-project="%s" """ % (appName)+\
          """--app="%s" """ % (appName)

	output, returncode = runCxFlow(cmd)
	cleanWorkspace(path)
	if returncode != 0:
		return HTTPError(body=output, status=520)
	else:
		return HTTPResponse(reason=output, status=200)


def prepareConfig():
	'''
	Prepare application.yml from the template, thios function exist for the case where we would
	like to dynamicly edit the template
	'''
	print("Preparing Application.yml", flush=True)
	f = open(applicationConfPath+".template", "r")
	template = f.read()
	f.close()
	f = open(applicationConfPath,"w")
	f.write(template)
	f.close()


def threadingScan():
    """ process jobs in queue """
    while True:
        while not q.empty():
            data = q.get()
            executer.submit(startCxFlowScanMode, data["config"],data["appName"],data["id"])
        time.sleep(1)

def downloadCxFlow(version):
	url = 'https://github.com/checkmarx-ltd/cx-flow/releases/download/%s/cx-flow-%s.jar' % (version, version)
	print("downloading cxFlow : %s" % url, flush=True) 
	r = requests.get(url)

	with open('cx-flow-%s.jar'% version, 'wb') as f:
		f.write(r.content)


def loadConfig():
	with open('conf/config.yml') as f:
		data = yaml.load(f, Loader=yaml.SafeLoader)
		return data


config = loadConfig()
applicationConfPath = "conf/cxflow-application.yml"

cxFlowJar = "cx-flow-%s.jar" % config['cxflow']['version']
if not (os.path.exists("cx-flow-%s.jar" % config['cxflow']['version'])):
	downloadCxFlow(config['cxflow']['version'])
print("Ready to go, Listen port %d" % config['port'],flush=True)

#-------------Initalisation-----------#

# Queue + thread are initalized for 'async' endpoints
q = Queue()
executer = ProcessPoolExecutor(max_workers=config['asyncThreads'])
t = threading.Thread(target=threadingScan)
t.setDaemon(True)
t.start()

#Waitress is a multi-threaded server, by default 4 threads are allowed
#app.run(host='0.0.0.0', port=config['port'], server='waitress', threads=1)
serve(app, host='0.0.0.0', port=config['port'], threads=config['httpTreads'])








